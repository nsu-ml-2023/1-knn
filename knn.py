from typing import Callable, Any
from collections import Counter

import numpy as np


class KNN:
    def __init__(self, k: int, distance: Callable[[np.ndarray, np.ndarray], float]):
        self._k = k
        self._distance = distance
        self._data: np.ndarray = np.zeros(0)
        self._labels: np.ndarray = np.zeros(0)

    def fit(self, x: np.ndarray, y: np.ndarray) -> None:
        assert x.shape[0] == y.shape[0]

        self._data = x
        self._labels = y

    def predict(
            self,
            x: np.ndarray,
            progress_hook: Callable[[int], None] | None = None,
            hook_period: int = 50
    ) -> np.ndarray:
        if x.shape == self._data.shape[1:]:
            x = x.reshape((1, ) + x.shape)

        assert x.shape[1:] == self._data.shape[1:]

        labels = np.zeros(shape=(x.shape[0],) + self._labels.shape[1:])

        for i, it in enumerate(x):
            labels[i] = self._predict_one(it)

            if progress_hook is not None and i % hook_period == 0:
                progress_hook(i)

        if progress_hook is not None:
            progress_hook(x.shape[0])

        return labels

    def _predict_one(self, x: np.ndarray) -> np.ndarray | Any:
        distances = np.array([self._distance(x, it) for it in self._data])
        indices = np.argpartition(distances, self._k)[:self._k]
        k_nearest = self._labels[indices]
        return Counter(k_nearest).most_common(1)[0][0]  # most_common -> [(<item>, <count>), ...]
