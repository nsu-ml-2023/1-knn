from typing import Callable, Any

import numpy as np

__all__ = [
    'GaussianRandomValuesGenerator',
    'InverseGaussianLookupTable',
    'gaussian_df',
    'generate_gaussian_cdf_table'
]


def gaussian_df(
        mean: float,
        stddev: float,
        x: float
) -> float:
    return 1 / stddev / np.sqrt(2 * np.pi) * np.exp(-0.5 * ((x - mean) / stddev) ** 2)


DEFAULT_STEP = 0.1


def generate_gaussian_cdf_table(
        mean: float,
        stddev: float,
        start: float | None = None,
        stop: float | None = None,
        step: float = DEFAULT_STEP
) -> tuple[np.ndarray, np.ndarray]:
    if start is None:
        start = mean + -4 * stddev
    if stop is None:
        stop = mean + 4 * stddev

    xs_ = np.arange(start, stop, step)
    ys_ = np.zeros(xs_.shape)
    acc = 0
    for i, x in enumerate(xs_):
        acc += gaussian_df(mean, stddev, x)
        ys_[i] = acc
    return xs_, ys_ * step


class InverseGaussianLookupTable:
    def __init__(
            self,
            mean: float,
            stddev: float,
            start: float | None = None,
            stop: float | None = None,
            step: float = DEFAULT_STEP
    ) -> None:
        self._xs, self._ys = generate_gaussian_cdf_table(mean, stddev, start, stop, step)

    def __call__(self, y: float) -> float:
        return self._xs[np.argmin(np.abs(self._ys - y))]


class GaussianRandomValuesGenerator:
    def __init__(
            self,
            mean: float,
            stddev: float,
            start: float | None = None,
            stop: float | None = None,
            step: float = DEFAULT_STEP,
            uniform_random_engine: Callable[[], float] = np.random.uniform
    ) -> None:
        self._inv_cdf = InverseGaussianLookupTable(mean, stddev, start, stop, step)
        self._random_engine = uniform_random_engine

    def __call__(self) -> Any:
        return self._inv_cdf(self._random_engine())
