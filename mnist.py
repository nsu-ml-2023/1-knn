import pickle
from typing import Final, Iterable

import numpy as np

_MNIST_PATH: Final[str] = 'MNIST-for-Numpy/mnist.pkl'


def load_all(binary_threshold: int | None = None) -> tuple[np.ndarray, np.ndarray]:
    """
    Credits: https://github.com/hsjeong5/MNIST-for-Numpy
    :return: (images, labels)
    """
    with open(_MNIST_PATH, mode='rb') as f:
        mnist = pickle.load(f)
    x, y = np.concatenate([mnist['training_images'], mnist['test_images']]), \
        np.concatenate([mnist['training_labels'], mnist['test_labels']])
    if binary_threshold is None:
        return x, y

    assert 0 <= binary_threshold <= 255
    return np.where(x <= binary_threshold, 0, 1).astype('bool'), y


def load(
        limit_train: int = -1,
        limit_test: int = -1,
        binary_threshold: int | None = None
) -> tuple[np.ndarray, np.ndarray, np.ndarray, np.ndarray]:
    """
    Credits: https://github.com/hsjeong5/MNIST-for-Numpy
    :return: (images, labels)
    """
    with open(_MNIST_PATH, mode='rb') as f:
        mnist = pickle.load(f)

    if binary_threshold is None:
        return (
            mnist['training_images'][:limit_train, :],
            mnist['test_images'][:limit_test, :],
            mnist['training_labels'][:limit_train],
            mnist['test_labels'][:limit_test]
        )

    assert 0 <= binary_threshold <= 255
    return (
        np.where(mnist['training_images'] <= binary_threshold, 0, 1).astype('bool')[:limit_train, :],
        np.where(mnist['test_images'] <= binary_threshold, 0, 1).astype('bool')[:limit_test, :],
        mnist['training_labels'][:limit_train],
        mnist['test_labels'][:limit_test]
    )


def load_labels(
        labels: Iterable[int],
        limit_train: int | None = None,
        limit_test: int | None = None,
        **kwargs
) -> tuple[np.ndarray, np.ndarray, np.ndarray, np.ndarray]:
    labels = list(labels)
    assert labels

    x_train, x_test, y_train, y_test = load(**kwargs)

    train_indices = (y_train == labels[0])
    test_indices = (y_test == labels[0])
    for digit in labels[1:]:
        train_indices = (train_indices | (y_train == digit))
        test_indices = (test_indices | (y_test == digit))

    if limit_train is not None:
        assert limit_train >= 0

        for digit in labels:
            selected_count = 0
            for i in range(train_indices.shape[0]):
                if y_train[i] != digit:
                    continue
                if selected_count >= limit_train // len(labels):
                    train_indices[i] = False
                if train_indices[i]:
                    selected_count += 1

    if limit_test is not None:
        assert limit_test >= 0

        for digit in labels:
            selected_count = 0
            for i in range(test_indices.shape[0]):
                if y_test[i] != digit:
                    continue
                if selected_count >= limit_test // len(labels):
                    test_indices[i] = False
                if test_indices[i]:
                    selected_count += 1

    return x_train[train_indices], x_test[test_indices], y_train[train_indices], y_test[test_indices]
