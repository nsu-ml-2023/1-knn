from __future__ import annotations

from typing import Callable, TypeAlias, Literal

import numpy as np

__all__ = [
    'to_binary',
    'apply',
    'apply_binary',
    'RandomEngine',
    'BinaryRandomEngine'
]


RandomEngine: TypeAlias = Callable[[], float]
BinaryRandomEngine: TypeAlias = Callable[[], Literal[-1, 0, 1]]


def to_binary(
        noise_generator: RandomEngine,
        low: float,
        high: float
) -> BinaryRandomEngine:
    def _generate() -> Literal[-1, 0, 1]:
        _value = noise_generator()
        if _value <= low:
            return -1
        if _value > high:
            return 1
        return 0

    return _generate


def apply(
        image: np.ndarray,
        random_engine: RandomEngine
) -> np.ndarray:
    pixel_count = image.ravel().shape[0]
    noise_texture = \
        np.array([random_engine() for _ in range(pixel_count)]) \
            .reshape(image.shape) \
            .astype('int32', copy=False)
    return np.clip(noise_texture + image, 0, 255).astype('uint8', copy=False).reshape(image.shape)


def apply_binary(
        image: np.ndarray,
        random_engine: BinaryRandomEngine,
) -> np.ndarray:
    assert image.dtype == np.dtype('bool')

    pixel_count = image.ravel().shape[0]
    noise_texture = \
        np.array([random_engine() for _ in range(pixel_count)]) \
            .reshape(image.shape) \
            .astype('int32', copy=False)
    return np.clip(noise_texture + image, 0, 1).astype('bool', copy=False).reshape(image.shape)
