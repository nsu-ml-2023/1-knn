from __future__ import annotations
import pickle
from dataclasses import dataclass
from pathlib import Path
from typing import Iterable, Callable, Any

import numpy as np


@dataclass
class TestParams:
    run_id: int
    metric_name: str
    image_type: str
    noise_type: str
    labels: Iterable[int]
    x_train: np.ndarray | Any
    x_test: np.ndarray | Any
    y_train: np.ndarray | Any
    y_test: np.ndarray | Any
    k: int
    metric: Callable[[np.ndarray | Any, np.ndarray | Any], np.ndarray | Any]


@dataclass
class TestResult:
    run_id: int
    metric_name: str
    image_type: str
    noise_type: str
    labels: Iterable[int]
    k: int
    y_pred: np.ndarray | Any
    y_true: np.ndarray | Any

    @staticmethod
    def load(path: Path) -> TestResult:
        with path.open(mode='wb') as f:
            return pickle.load(f)

    def dump(self, path: Path) -> None:
        with path.open(mode='wb') as f:
            pickle.dump(self, f)
