from typing import Any, Callable, Final

import numpy as np


def euclidian_distance(x: np.ndarray, y: np.ndarray) -> np.ndarray | Any:
    x = x.astype('int32', copy=False)
    y = y.astype('int32', copy=False)
    return np.sqrt(euclidian_distance_squared(x, y))


def euclidian_distance_squared(x: np.ndarray, y: np.ndarray) -> np.ndarray | Any:
    x = x.astype('int32', copy=False)
    y = y.astype('int32', copy=False)
    return np.sum(np.square(x - y), axis=-1)


def manhattan_distance(x: np.ndarray, y: np.ndarray) -> np.ndarray | Any:
    x = x.astype('int32', copy=False)
    y = y.astype('int32', copy=False)
    return np.sum(np.abs(x - y), axis=-1)


def weighted_euclidian_distance(
        w: np.ndarray,
        x: np.ndarray,
        y: np.ndarray
) -> np.ndarray | Any:
    x = x.astype('int32', copy=False)
    y = y.astype('int32', copy=False)
    return np.sqrt(np.sum(w * np.square(x - y), axis=-1))


def chebyshev_distance(x: np.ndarray, y: np.ndarray) -> np.ndarray | Any:
    x = x.astype('int32', copy=False)
    y = y.astype('int32', copy=False)
    return np.max(np.abs(x - y), axis=-1)


METRICS: Final[tuple[tuple[str, Callable[[...], np.ndarray | Any]], ...]] = (
    ('euclidian_distance', euclidian_distance),
    ('euclidian_distance_squared', euclidian_distance_squared),
    ('manhattan_distance', manhattan_distance),
    ('weighted_euclidian_distance', weighted_euclidian_distance),
    ('chebyshev_distance', chebyshev_distance),
)
